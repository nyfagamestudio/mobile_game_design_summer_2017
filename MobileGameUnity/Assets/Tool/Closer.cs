﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Closer : MonoBehaviour
{
    public GameObject ExportWindow;
    public GameObject ImportWindow;

    // Use this for initialization
    void Start()
    {

    }

    public void TaskOnClick()
    {
        ExportWindow.SetActive(false);
        ImportWindow.SetActive(false);
    }
}

