﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCreationMenu : MonoBehaviour {
    public GameObject ExportWindow;
    public GameObject ImportWindow;
	public Dropdown fileDropdown;
	public Dropdown cellType;
	public Dropdown powerUp;
	public InputField RowField;
	public InputField ColumnField;
	public InputField HPField;
	public InputField LevelNameField;
	public GridCellWidget[] m_CellWidgets;
	private GridCellWidget SelectedCell;
    // Use this for initialization
    void Start () {
        //Debug.Log("Start");
    }
	
	// Update is called once per frame
	void Update ()
	{
		/*if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			SaveLevel("_test");
		}

		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			LoadLevel("_test");
		}*/
    }

    public void OnFileOptionChosen(int option)
    {
		if (option == 0)
		{
			//ImportWindow.SetActive(true);
			LoadLevel(LevelNameField.text);

		}
		else if (option == 1)
        {
            Debug.Log("import");
            //ImportWindow.SetActive(true);
			LoadLevel(LevelNameField.text);
			fileDropdown.value = 0;
        }
        else if (option == 2)
        {
            Debug.Log("export");
            //ExportWindow.SetActive(true);
			SaveLevel(LevelNameField.text);
			fileDropdown.value = 0;
        }
    }

	public void GridCellClicked (GridCellWidget Cell)
	{
		Debug.Log (Cell.Column + " , " + Cell.Row + " " + Cell.Cell.bombValue + " " + Cell.Cell.cellType + " " + Cell.Cell.powerup);
		SelectedCell = Cell;
		RowField.text = Cell.Row.ToString();
		ColumnField.text = Cell.Column.ToString();
		HPField.text = Cell.Cell.bombValue.ToString();
		cellType.value = (int)Cell.Cell.cellType;
		powerUp.value = (int)Cell.Cell.powerup;
	}
	public void OnCellTypeChosen(int choice)
	{
		if (choice == 0) {
			HPField.text = "0";
			SelectedCell.Cell.bombValue = 0;
		}
		else if (choice == 2) {
			HPField.text = "0";
			SelectedCell.Cell.bombValue = 0;
		}
		else if (choice == 3) {
			HPField.text = "0";
			SelectedCell.Cell.bombValue = 0;
		}
		else if (choice == 4) {
			HPField.text = "0";
			SelectedCell.Cell.bombValue = 0;
		}
		SelectedCell.Cell.cellType = (GridCell.type)choice;
		SelectedCell.UpdateInformation ();
		DisableDropDown ();
	}
	public void OnPowerupTypeChosen(int choice)
	{
		SelectedCell.Cell.powerup = (GridCell.powerupType)choice;
		SelectedCell.UpdateInformation ();
	}
	public void OnHealthValueChanged(string health)
	{
		SelectedCell.Cell.bombValue = int.Parse (health);
		SelectedCell.UpdateInformation ();
	}

	GridCell[] ConvertWidgetsToGridCells(GridCellWidget[] widgets)
	{
		GridCell[] cells = new GridCell[widgets.Length];
		for (int i = 0; i < widgets.Length; i++)
		{
			cells[i] = widgets[i].Cell;
		}
		return cells;
	}

	string GetFilePath(string fileName)
	{
		return Application.dataPath + "/Resources/Levels/" + fileName + ".json";
	}

	public void SaveLevel(string fileName)
	{
		if (string.IsNullOrEmpty(fileName))
		{
			Debug.LogError("Failed to save: Enter a filename in the Level Name input field");
			return;
		}
		GridToJson io = new GridToJson();
		io.WriteGrid(ConvertWidgetsToGridCells(m_CellWidgets), GetFilePath(fileName));
	}

	public void LoadLevel(string fileName)
	{
		if (string.IsNullOrEmpty(fileName))
		{
			Debug.LogError("Failed to load: Enter a filename in the Level Name input field");
			return;
		}
		JsonToGrid io = new JsonToGrid();
		GridCell[] cells = io.GetGrid(GetFilePath(fileName));

		for (int i = 0; i < m_CellWidgets.Length; i++)
		{
			m_CellWidgets[i].Cell = cells[i];
			m_CellWidgets[i].UpdateInformation ();
		}
	}
	public void DisableDropDown()
	{
		if (cellType.value == (int)GridCell.type.Currency) 
		{
			powerUp.interactable = false;
		} 
		else if (cellType.value == (int)GridCell.type.Ammo) 
		{
			powerUp.interactable = false;

		}
		else if (cellType.value == (int)GridCell.type.Empty) 
		{
			powerUp.interactable = false;

		}
		else 
		{
			powerUp.interactable = true;
		}
	}
}