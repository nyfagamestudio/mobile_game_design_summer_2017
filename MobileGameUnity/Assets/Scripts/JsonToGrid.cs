﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class JsonToGrid{

	private string jsonString;
    public GridCell[] gridArray = new GridCell[350];
    public int ReadGrid(Grid grid, TextAsset level)
	{
        int bombCount = 0;
        jsonString = level.text;
		//jsonString = File.ReadAllText("Assets/Levels/" + level + ".json"); //Change path to Application.dataPath before building for release
		gridArray = JsonHelper.FromJson<GridCell>(jsonString);
        int cellCount = 0;
        for (int i = 0; i < 50; i++)
        {
            for (int k = 0; k < 7; k++)
            {
                GridCell cell;
                cell = new GridCell(gridArray[cellCount].bombValue, gridArray[cellCount].currency, gridArray[cellCount].powerup, gridArray[cellCount].cellType);
				if (gridArray[cellCount].cellType == GridCell.type.Bomb || gridArray[cellCount].cellType == GridCell.type.Taunt || gridArray[cellCount].cellType == GridCell.type.Infected|| gridArray[cellCount].cellType == GridCell.type.Portal)
                {
                    bombCount++;
                }
                grid.SetCell(k, i, cell);
                cellCount++;
            }
        }
        return bombCount;
	}

    public GridCell[] GetGrid(string path)
    {
        jsonString = File.ReadAllText(path);
        gridArray = JsonHelper.FromJson<GridCell>(jsonString);
        return gridArray;
    }
}
