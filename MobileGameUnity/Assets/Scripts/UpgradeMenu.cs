﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour {
	public Text MoneyText;
	public GameObject Upgrades;
    public GameObject MainMenu;
	public ItemSaves.UpgradeCategory Reset;
	public ItemsScript [] ItemLocks;

	// Use this for initialization
	void Start () 
	{
		PlayerPrefs.SetInt( "DefaultBall" , 1);
		PlayerPrefs.SetInt( "DefaultGun" , 1);
	}

	void Update()
	{
		MoneyText.text = GameWallet.GetTotalCurrency ().ToString();
	}
    public void MenuPressed()
    {
        Upgrades.SetActive(false);
        MainMenu.SetActive(true);           
    }

	public void Lock()
	{
		for (int i = 0; i < ItemLocks.Length; i++) 
		{
			ItemLocks [i].Reset ();
		}
		ItemSaves.SetLastUpgradeChosen (-1, ItemSaves.UpgradeCategory.BallColor);
		ItemSaves.SetLastUpgradeChosen (-1, ItemSaves.UpgradeCategory.GunColor);
		ItemSaves.SetLastUpgradeChosen (-1, ItemSaves.UpgradeCategory.Traj);
	}


}
