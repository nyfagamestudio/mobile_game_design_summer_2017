﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootBall : MonoBehaviour {

    public Grid grid;
    public GameObject ball;
    public GameObject bomb;
    public GameObject gun;
    public GameObject rotatePoint;
	public GameObject mirrorObject;
	public GameObject lineRendererBall1;
	public GameObject lineRendererBall2;
	public GameObject lineRendererBall3;
    public Transform ballStart;
    private Vector3 startPos;
    public int ammo = 1;
    public int gainedAmmo;
    public int returnedAmmo;
    public int mirrorAmmo;
    public LineRenderer line;
    
    private GameObject resetGun;

    private bool shooting = false;
    private bool aiming = false;
    public bool instaKill;
	public int aimUpgradeCount;
    public int savedInstaKill;
    public bool piercing;
    public int savedPiercing;
    public int doubleDamage;
    public int savedDoubleDamage;
	public bool mirror;
	public int savedMirror;
	public int mirrorCounter = 0;
	public bool moveableGun;
	public int savedMoveableGun;
	public bool gunMoving;
    public AudioClip shootsound;
    public AudioClip powerupActivateSound;
    AudioSource audioSource;
	SoundManager soundManager;

    //Powerup buttons
    public GameObject ddFlash;
    public GameObject ikFlash;
    public GameObject pFlash;
    public GameObject mFlash;
    public GameObject movegunFlash;
    public Button ddButton;
	public Button ikButton;
	public Button pButton;
	public Button mButton;
	public Button movegunButton;
    public Text ddText;
    public Text ikText;
    public Text pText;
    public Text mText;
    public Text movegunText;
	public Text ammoText;

    //Testing
    public EndlessGridTest endless;

    // Use this for initialization
    private void Start()
    {
        if (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.Traj) == "")
        {
            aimUpgradeCount = 0;
        }
        else if (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.Traj) == "TrajectoryGuide1")
        {
            aimUpgradeCount = 1;
        }
        else if (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.Traj) == "TrajectoryGuide2")
        {
            aimUpgradeCount = 2;
        }
        ammoText.text = ammo.ToString ();
        line.SetPosition(0, rotatePoint.transform.position);
        audioSource = GetComponent<AudioSource>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
        resetGun = new GameObject();
        resetGun.transform.position = gun.transform.position;
        resetGun.transform.rotation = gun.transform.rotation;
        resetGun.transform.localScale = gun.transform.localScale;
		mirrorObject.SetActive (false);
        //SpawnCells ();
    }
    void Update()
    {
		if (savedMirror > 0) {
			mButton.interactable = true;
		}
        if (savedInstaKill > 0)
        {
            ikButton.interactable = true;
        }
        if(savedMoveableGun > 0)
        {
            movegunButton.interactable = true;
        }
		if (!shooting)
        {
                if (Input.GetMouseButtonDown(0))
                {
                    startPos = transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                    transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                    line.enabled = true;
                    aiming = true;
                }
                if (Input.GetMouseButton(0))
                {
                    transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
                    Vector2 limiter = (startPos - transform.position);
                    if (limiter.y > 0.5 || (limiter.x > 0.5 && limiter.x < -0.5 && limiter.y > 0))
                    {
                        aiming = true;
                        line.enabled = true;
                        Aiming((startPos - transform.position));
                    }
                    else
                    {
                        aiming = false;
                        line.enabled = false;
						lineRendererBall1.transform.position = new Vector3 (100, 100, 100);
						lineRendererBall2.transform.position = new Vector3 (100, 100, 100);
						lineRendererBall3.transform.position = new Vector3 (100, 100, 100);
                    }
                }
                if (aiming)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
						lineRendererBall1.transform.position = new Vector3 (100, 100, 100);
						lineRendererBall2.transform.position = new Vector3 (100, 100, 100);
						lineRendererBall3.transform.position = new Vector3 (100, 100, 100);
                        /*ddButton.interactable = false;
                        ikButton.interactable = false;
                        pButton.interactable = false;
                        mButton.interactable = false;
                        movegunButton.interactable = false;*/
                        shooting = true;
                        StartCoroutine(Shoot());
                        line.enabled = false;
                    }
                }
        }
		if (moveableGun) 
		{
			if (Input.GetMouseButtonDown(0))
			{
                startPos = transform.position;// = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
				transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
				line.enabled = true;
				aiming = true;
			}
			if (Input.GetMouseButton(0))
			{
				transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
				Vector2 limiter = (startPos - transform.position);
				if (limiter.y > 0)
				{
					aiming = true;
					line.enabled = true;
					Aiming((startPos - transform.position));
				}
				else
				{
					aiming = false;
					line.enabled = false;
				}
			}
			if (Input.GetMouseButtonUp (0)) {
				line.enabled = false;
			}
		}

            if (returnedAmmo >= ammo)
            {
				line.enabled = false;
				gunMoving = false;
           	 	gun.transform.Find("MovingGun").gameObject.SetActive(false);
            	moveableGun = false;
                shooting = false;
                ammo += gainedAmmo;
                gainedAmmo = 0;
                returnedAmmo = 0;
				ammoText.text = ammo.ToString();
                grid.LowerGrid();
                if (grid.endless)
                {
                    endless.extraSpawned = false;
                    endless.SpawnLine();
                }
				if (doubleDamage > 0) {
					doubleDamage = 0;
				}
				if (piercing) {
					piercing = false;
				}
				if (mirror) 
				{
					mirror = false;
				}
                gun.transform.position = resetGun.transform.position;
				gun.transform.rotation = resetGun.transform.rotation;
				//PowerupButtons ();
				for (int i = 0; i < 10; i++) {
					for (int k = 0; k < 7; k++) {
						if (grid.gameGrid [k, i].cellType == GridCell.type.Taunt) 
						{
						GameObject[] bombs;
						GameObject[] infecteds;
						GameObject[] portals;
						bombs = GameObject.FindGameObjectsWithTag ("Bomb");
						infecteds = GameObject.FindGameObjectsWithTag ("Infected");
						portals = GameObject.FindGameObjectsWithTag ("Portal");
						foreach (GameObject bomb in bombs) 
						{
							bomb.GetComponent<Bomb> ().immune = true;
						}
						foreach (GameObject infect in infecteds) {
							infect.GetComponent<Bomb> ().immune = true;
						}
						foreach (GameObject portal in portals) {
							portal.GetComponent<Bomb> ().immune = true;
						}
						}
					}
				}
                //LowerCells ();
                //SpawnCells ();
            }
        


    }

    void Aiming(Vector2 direction)
    {
		LineRenderer lineRenderer = GetComponent<LineRenderer> ();
		//Upgrade Counter
		lineRenderer.positionCount = aimUpgradeCount + 2;
        line.SetPosition(0, rotatePoint.transform.position);
		LayerMask layermask = ~((1 << LayerMask.NameToLayer ("Powerups"))|(1 << LayerMask.NameToLayer("Balls")));

		RaycastHit2D aim = Physics2D.CircleCast (rotatePoint.transform.position, 0.3f, direction, 50f, layermask);
		//Upgrade 1a
		Vector2 bounce1 = Vector2.Reflect(direction, aim.normal);
		RaycastHit2D aim2 = Physics2D.CircleCast(aim.centroid, 0.3f, bounce1 , 100f , layermask);
		//Upgrade 2
		RaycastHit2D aim3 = Physics2D.CircleCast(aim2.centroid, 0.3f, Vector2.Reflect(bounce1, aim2.normal), 100f, layermask);
        Vector3 aimDirection = line.GetPosition(1) - line.GetPosition(0);
        aimDirection.z = 0;
        gun.transform.forward = -aimDirection;
        float tempXRotation = Mathf.DeltaAngle(0, gun.transform.localEulerAngles.x);

        if (aim.collider != null)
        {
			lineRendererBall1.transform.position = new Vector3 (aim.centroid.x, aim.centroid.y, 0);
			line.SetPosition(1, new Vector3(aim.centroid.x, aim.centroid.y, 0));
			if (aimUpgradeCount > 0) {
				if (aim2.collider != null) {
					if (aim2.collider.name != "Deathzone") {
						lineRendererBall2.transform.position = new Vector3 (aim2.centroid.x, aim2.centroid.y, 0);
						line.SetPosition (2, new Vector3 (aim2.centroid.x, aim2.centroid.y, 0));
						if (aim3.collider != null) {
							if (aimUpgradeCount > 1) {
								if (aim3.collider.name != "Deathzone") {
									lineRendererBall3.transform.position = new Vector3 (aim3.centroid.x, aim3.centroid.y, 0);
									line.SetPosition (3, new Vector3 (aim3.centroid.x, aim3.centroid.y, 0));
								} else {
									line.SetPosition (3, new Vector3 (aim2.centroid.x, aim2.centroid.y, 0));
									lineRendererBall3.transform.position = new Vector3 (100, 100, 100);
								}
							}
						}
					}
                    else
                    {
                        if (aimUpgradeCount > 0)
                        {
                            line.SetPosition(2, new Vector3(aim.point.x, aim.point.y, 0));
                            lineRendererBall2.transform.position = new Vector3(100, 100, 100);
                            if (aimUpgradeCount > 1)
                            {
                                line.SetPosition(3, new Vector3(aim.point.x, aim.point.y, 0));
                                lineRendererBall3.transform.position = new Vector3(100, 100, 100);
                            }
                        }

                    }
                } 
			}
        }
        if (aimDirection.x <= 0)
        {
            gun.transform.rotation = Quaternion.Euler(tempXRotation, 90, -90);
        }
        else if (aimDirection.x > 0)
        {
            gun.transform.rotation = Quaternion.Euler(180-tempXRotation, 90, -90);
        }
    }

    IEnumerator Shoot()
    {
		int ammoCounter = ammo;
        for (int i = 0; i < ammo; i++) {
			ammoCounter -= 1;
			ammoText.text = ammoCounter.ToString ();

			audioSource.pitch = Random.Range(0.8f,1.2f);
			audioSource.PlayOneShot (shootsound, soundManager.volume);
			if (instaKill) {
				GameObject ballInstance;
                ballInstance = Instantiate (ball, ballStart.position, transform.rotation) as GameObject;
                BallScript ballScript = ballInstance.GetComponent<BallScript>();
                ballScript.SetBallColor(ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
                Transform[] trails = ballInstance.transform.Find("Trails").gameObject.GetComponentsInChildren<Transform>();
                foreach(Transform child in trails)
                {
                    child.gameObject.SetActive(false);
                }
                ballInstance.transform.Find("Trails").gameObject.SetActive(true);
                ballInstance.transform.Find("Trails").transform.Find("Instakill").gameObject.SetActive(true);
				ballScript.SetVelocity ((startPos - transform.position));
				ballScript.instaKill = true;
				instaKill = false;
				yield return new WaitForSeconds (0.25f);
                
			} else if (piercing) {
				GameObject ballInstance;
                ballInstance = Instantiate (ball, ballStart.position, transform.rotation) as GameObject;
                BallScript ballScript = ballInstance.GetComponent<BallScript>();
                ballScript.SetBallColor(ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
                Transform[] trails = ballInstance.transform.Find("Trails").gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in trails)
                {
                    child.gameObject.SetActive(false);
                }
                ballInstance.transform.Find("Trails").gameObject.SetActive(true);
                ballInstance.transform.Find ("Trails").transform.Find("Piercing").gameObject.SetActive (true);
				ballScript.SetVelocity ((startPos - transform.position));
				ballScript.piercing = true;
				if (doubleDamage > 0) {
                    ballInstance.transform.Find("Trails").transform.Find("2x Damage").gameObject.SetActive(true);
                    ballScript.doubleDamage = doubleDamage;
				}
				yield return new WaitForSeconds (0.25f);
			} else if (doubleDamage > 0) {
				GameObject ballInstance;
                ballInstance = Instantiate (ball, ballStart.position, transform.rotation) as GameObject;
                BallScript ballScript = ballInstance.GetComponent<BallScript>();
                ballScript.SetBallColor(ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
                Transform[] trails = ballInstance.transform.Find("Trails").gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in trails)
                {
                    child.gameObject.SetActive(false);
                }
                ballInstance.transform.Find("Trails").gameObject.SetActive(true);
                ballInstance.transform.Find ("Trails").transform.Find("2x Damage").gameObject.SetActive (true);
				ballScript.SetVelocity ((startPos - transform.position));
				ballScript.doubleDamage = doubleDamage;
				//ballScript.SetBallColor (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
				yield return new WaitForSeconds (0.25f);
			} else if (mirror) {
				GameObject ballInstance;
                ballInstance = Instantiate (ball, ballStart.position, transform.rotation) as GameObject;
				BallScript ballScript = ballInstance.GetComponent<BallScript> ();
				ballScript.SetVelocity ((startPos - transform.position));
				if (doubleDamage > 0) {
					ballScript.doubleDamage = doubleDamage;
				}
				ballScript.SetBallColor (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
				yield return new WaitForSeconds (0.25f);
			}
            else
            {
                GameObject ballInstance;
                ballInstance = Instantiate(ball, ballStart.position, transform.rotation) as GameObject;
                BallScript ballScript = ballInstance.GetComponent<BallScript>();
                ballScript.SetVelocity((startPos - transform.position));
				ballScript.SetBallColor (ItemSaves.GetLastUpgradeChosenName(ItemSaves.UpgradeCategory.BallColor));
                yield return new WaitForSeconds(0.25f);
            }
		}
	}

	public void ActivatePowerup(int powerup)
	{
		if (powerup == 1) {
			if (instaKill == false) 
			{
				instaKill = true;
				savedInstaKill -= 1;
                ikText.text = savedInstaKill.ToString();
				audioSource.PlayOneShot(powerupActivateSound, soundManager.volume);
                if (savedInstaKill == 0) 
				{
					ikButton.interactable = false;
				}
			}
		} else if (powerup == 2) {
			if (piercing == false) 
			{
				piercing = true;
				savedPiercing -= 1;
                pText.text = savedPiercing.ToString();
				audioSource.PlayOneShot(powerupActivateSound, soundManager.volume);
                if (savedPiercing == 0) 
				{
					pButton.interactable = false;
				}
			}
		} else if (powerup == 3) {
			doubleDamage += 1;
			savedDoubleDamage -= 1;
            ddText.text = savedDoubleDamage.ToString();
			audioSource.PlayOneShot(powerupActivateSound, soundManager.volume);
            if (savedDoubleDamage == 0) 
			{
				ddButton.interactable = false;
			}
		}
		else if (powerup == 4) {
			if (mirror == false) 
			{
				mirrorObject.SetActive (true);
				mirror = true;
                mirrorAmmo = ammo - returnedAmmo;
                savedMirror -= 1;
                mText.text = savedMirror.ToString();
				audioSource.PlayOneShot(powerupActivateSound, soundManager.volume);
                if (savedMirror == 0) {
					mButton.interactable = false;
				}
			}
		}
		else if (powerup == 5) {
			if (moveableGun == false) 
			{
				moveableGun = true;
				savedMoveableGun -= 1;
                movegunText.text = savedMoveableGun.ToString();
				audioSource.PlayOneShot(powerupActivateSound, soundManager.volume);
                gun.transform.Find("MovingGun").gameObject.SetActive(true);
                if (savedMoveableGun == 0) {
					movegunButton.interactable = false;
				}
			}
		}
	}

	/*public void PowerupButtons()
	{
		if (savedDoubleDamage > 0) 
		{
			ddButton.interactable = true;
            ddText.text = savedDoubleDamage.ToString();
		}
		if (savedInstaKill > 0) 
		{
			ikButton.interactable = true;
            ikText.text = savedInstaKill.ToString();
        }
		if (savedPiercing > 0) 
		{
			pButton.interactable = true;
            pText.text = savedPiercing.ToString();
        }
		if (savedMirror > 0) 
		{
			mButton.interactable = true;
            mText.text = savedMirror.ToString();
        }
        if (savedMoveableGun > 0)
        {
            movegunButton.interactable = true;
            movegunText.text = savedMoveableGun.ToString();
        }
    }*/
    public void PowerupPickup(int puID)
    {
        switch (puID)
        {
            case 1:
                ddFlash.SetActive(true);
                ddButton.interactable = true;
                ddText.text = savedDoubleDamage.ToString();
                StartCoroutine(PowerupAnimControl(puID));
                break;
            case 2:
                pFlash.SetActive(true);
                pButton.interactable = true;
                pText.text = savedPiercing.ToString();
                StartCoroutine(PowerupAnimControl(puID));
                break;
            case 3:
                ikFlash.SetActive(true);
                ikButton.interactable = true;
                ikText.text = savedInstaKill.ToString();
                StartCoroutine(PowerupAnimControl(puID));
                break;
            case 4:
                mFlash.SetActive(true);
                mButton.interactable = true;
                mText.text = savedMirror.ToString();
                StartCoroutine(PowerupAnimControl(puID));
                break;
            case 5:
                movegunFlash.SetActive(true);
                movegunButton.interactable = true;
                movegunText.text = savedMoveableGun.ToString();
                StartCoroutine(PowerupAnimControl(puID));
                break;
            default:
                break;
        }
    }

    IEnumerator PowerupAnimControl(int puID)
    {
        yield return new WaitForSeconds(0.5f);
        switch (puID)
        {
            case 1:
                ddFlash.SetActive(false);
                break;
            case 2:
                pFlash.SetActive(false);
                break;
            case 3:
                ikFlash.SetActive(false);
                break;
            case 4:
                mFlash.SetActive(false);
                break;
            case 5:
                movegunFlash.SetActive(false);
                break;
            default:
                break;
        }
    }
	public void DeactivateMirror()
	{
		mirrorObject.SetActive (false);
	}
    /*void SpawnCells()
	{
		for (int i = 0; i < 7; i++) {
			int chance = Random.Range (0, 5);
			if (chance == 3) {
				Instantiate (bomb, new Vector2 (-4.41f + i * 1.46f, 7.7f), Quaternion.identity);
			}
		}
	}

	void LowerCells()
	{
		GameObject[] squares = GameObject.FindGameObjectsWithTag ("Bomb");
		foreach (GameObject square in squares) {
			square.transform.position = new Vector2 (square.transform.position.x, square.transform.position.y - 1.447f);
		}
	}*/
}
