﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class GameGridObject : MonoBehaviour{
    public int row;
    public int col;
    public Grid grid;

    virtual public void Start()
    {
        grid = GameObject.Find("Grid").GetComponent<Grid>();
    }
}
