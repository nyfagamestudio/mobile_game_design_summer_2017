﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : GameGridObject
{
    public ShootBall ballPowerup;
    public GridCell.powerupType powerup;

    void Awake()
    {
        ballPowerup = GameObject.Find("Shooter").GetComponent<ShootBall>();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ball")
        {
            switch (powerup)
            {
                case (GridCell.powerupType.DoubleDamage):
                    ballPowerup.savedDoubleDamage += 1;
                    ballPowerup.PowerupPickup(1);
                    break;
                case (GridCell.powerupType.Piercing):
                    ballPowerup.savedPiercing += 1;
                    ballPowerup.PowerupPickup(2);
                    break;
                case (GridCell.powerupType.InstantBomb):
                    ballPowerup.savedInstaKill += 1;
                    ballPowerup.PowerupPickup(3);
                    break;
				case (GridCell.powerupType.Mirror):
					ballPowerup.savedMirror += 1;
                    ballPowerup.PowerupPickup(4);
                    break;
				case (GridCell.powerupType.ShootNMove):
					ballPowerup.savedMoveableGun += 1;
                    ballPowerup.PowerupPickup(5);
                    break;
                default:
                    break;
            }
            grid.EmptyCell(row, col);
            Destroy(gameObject);
        }
    }
}