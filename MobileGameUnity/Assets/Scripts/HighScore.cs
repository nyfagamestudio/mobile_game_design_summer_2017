﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour{
    public int[] HighScoreArray = new int[5];
    public string[] HighScoreName = new string[5];

    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            HighScoreArray[i] = PlayerPrefs.GetInt("Score" + (i + 1), 0);
        }
        for (int k = 0; k < 5; k++)
        {
            HighScoreName[k] = PlayerPrefs.GetString("Name" + (k + 1), "");
        }
    }

    public void CheckHighscore(int score, string name)
    {
        int tempInt = 0;
        int switchInt = 0;
        string tempString = "";
        string switchString = "";
        bool scoreUpdate = false;
        for (int i = 0; i < 5; i++)
        {
            if (scoreUpdate)
            {
                switchInt = HighScoreArray[i];
                HighScoreArray[i] = tempInt;
                PlayerPrefs.SetInt("Score" + (i + 1), HighScoreArray[i]);
                tempInt = switchInt;
                switchString = HighScoreName[i];
                HighScoreName[i] = tempString;
                PlayerPrefs.SetString("Name" + (i + 1), HighScoreName[i]);
                tempString = switchString;
            }
            if (score > HighScoreArray[i] && !scoreUpdate)
            {
                tempInt = HighScoreArray[i];
                tempString = HighScoreName[i];
                HighScoreArray[i] = score;
                PlayerPrefs.SetInt("Score" + (i + 1), score);
                HighScoreName[i] = name;
                PlayerPrefs.SetString("Name" + (i + 1), name);
                scoreUpdate = true;
            }
            
        }
    }

    public bool IsHighScore(int score)
    {
        bool isHighScore = false;
        for (int i = 0; i < 5; i++)
        {
            if(score > HighScoreArray[i])
            {
                isHighScore = true;
            }
        }
        return isHighScore;
    }
}
