﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelect : MonoBehaviour {
	
	public GameObject UpView;
	public Button [] Colors;
		// Use this for initialization
	void Start () 
	{
		

		foreach (Button m in Colors) 
		{
			if (m != null) 
			{
				m.enabled = true;
			}

		}
	}
		
}
