﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory{
    public int piercing;
    public int instaKill;
    public int moving;
    public int doubleDamage;
    public int mirror;
}
