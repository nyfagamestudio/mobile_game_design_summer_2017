﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : GameGridObject {
	public ShootBall shooter;
    public AudioClip ammosound;
    AudioSource audioSource;
	SoundManager soundManager;

    // Use this for initialization
    override public void Start () {
        base.Start();
        if (GameObject.Find("Shooter") != null)
        {
            shooter = GameObject.Find("Shooter").GetComponent<ShootBall>();
        }
        audioSource = GetComponent<AudioSource>();
		soundManager = GameObject.Find ("SoundManager").GetComponent<SoundManager> ();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Ball") {
			grid.EmptyCell (row, col);
			shooter.gainedAmmo += 1;
            audioSource.pitch = Random.Range(1f, 1f);
			audioSource.PlayOneShot(ammosound, soundManager.volume);
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
            transform.Find("Circle").gameObject.SetActive(false);
            transform.Find("Inside").gameObject.SetActive(false);
            transform.Find("Beams").gameObject.SetActive(false);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, ammosound.length);
		}
	}
}
