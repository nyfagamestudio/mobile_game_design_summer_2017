﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    bool isPaused = false;
    bool fastForwardBlocked = false;
    bool fastForward = false;
    public GameObject Shooter;
    public GameObject Canvas;
    public GameObject GameUI;
    public GameObject Help;
    public GameObject FastForwardBlock;
    public Toggle FastForwardButton;

    // Use this for initialization
    void Start () {
        Shooter = GameObject.Find("Shooter");
		Canvas = GameObject.Find("HelpScreen");
        GameUI = GameObject.Find("Game UI");
		Help = GameObject.Find("HelpMenu");
        FastForwardBlock = GameObject.Find("FastForwardBlock");
        FastForwardBlock.SetActive(false);
        Help.SetActive(false);

        Canvas.SetActive(!Canvas.activeInHierarchy);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Shooter.SetActive(!Shooter.activeInHierarchy);
            Canvas.SetActive(!Canvas.activeInHierarchy);
            

            isPaused = !isPaused;
            fastForwardBlocked = !fastForwardBlocked;
            if (isPaused)
            {
                Time.timeScale = 0;
            }
            else { 
                Time.timeScale = 1;
            }
            if (fastForwardBlocked)
            {
                FastForwardBlock.SetActive(true);
            }
            else { 
                FastForwardBlock.SetActive(false);
            }
        }
    }
    
    public void Pause()
    {
        if (isPaused) {
            Time.timeScale = 1;
            FastForwardButton.isOn = true;
        }
        else { Time.timeScale = 0; }
        if (fastForwardBlocked)
        {
            FastForwardBlock.SetActive(false);
        }
        else { FastForwardBlock.SetActive(true);  }
        Shooter.SetActive(!Shooter.activeInHierarchy);
        Canvas.SetActive(!Canvas.activeInHierarchy);
        isPaused = !isPaused;
        fastForwardBlocked = !fastForwardBlocked;

    }

    public void ResetPlayerPrefs()
    {
        for (int i = 0; i < 6; i++)
        {
            PlayerPrefs.SetInt("Score" + (i + 1), 0);
            PlayerPrefs.SetString("Name" + (i + 1), "");
        }
    }

    public void HelpMenu()
    {
        Help.SetActive(true);
    }

    public void Back()
    {
        Help.SetActive(false);
    }

    public void FastForward()
    {
        if (fastForward)
        {
            Time.timeScale = 1;
        }
        else { Time.timeScale = 2; }
        fastForward = !fastForward;
    }
}

